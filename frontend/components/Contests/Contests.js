import React from "react";
import FadeInSection from "../FadeIn/FadeIn";
import {faChevronLeft, faChevronRight, faPlus, faSearch} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import contests from '../../data/contests.json';

export default function Contests() {
    return (
        <div className={"contests-page"}>
            <FadeInSection key={2}>

                <div className={"title blue"}>Конкурсы и гранты</div>

                <div className={"blocks"}>

                    <div className={"blocks-description"}>
                        <div className={"b1"}>Наименование конкурса</div>
                        <div className={"b2"}>Прием заявок</div>
                        <div className={"b3"}>Статус конкурса</div>
                    </div>
                    {contests.map(({url, name, task, startdata, enddata, startimplementation, endimplementation, status, maxgrantsize,mingrantsize}) => {
                        return (
                    <div className={"block-row"}>
                        <div className={"description"}>
                        <b>{name}</b> <br></br><br></br> <b>Задача:</b> {task}
                        </div>

                        <div className={"reception"}>
                            {startdata && enddata != '' 
                            ? <span className={"active"}>от {startdata} <br/>до {enddata}</span>
                            : <span className={"active"}>мероприятие уже идёт</span>}
                            <br></br><button><a href={url}>Подать заявку</a></button>
                        </div>

                        
                            {status == 1 
                            ? <div className={"status"}>
                            <div className={"status-s active"}>
                                Прием заявок
                            </div>
                            <div className={"status-s"}>
                                Экспертиза
                            </div>
                            <div className={"status-s"}>
                                Конкурс завершен
                            </div>
                        </div>
                            : <div className={"status"}>
                            <div className={"status-s"}>
                                Прием заявок
                            </div>
                            <div className={"status-s active-a"}>
                                Экспертиза
                            </div>
                            <div className={"status-s"}>
                                Конкурс завершен
                            </div>
                        </div>}
                    </div>
                    )})}
                </div>
            </FadeInSection>
        </div>
    )
}