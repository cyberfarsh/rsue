import React from "react";
import FadeInSection from "../FadeIn/FadeIn";
import { faChevronLeft, faChevronRight, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { useState, useEffect } from 'react'
import ReactPaginate from 'react-paginate';

export default function People({ data }) {
    const [cards, setCards] = useState([])
    const router = useRouter()
    const selectedPage = useRef(0)
    const [query, setQuery] = useState("")
    const [isDisabled, setDisabled] = useState(true)
    const [isDotsActive, setDotsActive] = useState(false)
    const [filterArea, setFilterArea] = useState("")
    const [filterStatus, setFilterStatus] = useState("")
    const [isFetching, setFetching] = useState(true)

    //Проверяем, что данные есть
    useEffect(() => {
        if (data) {
            const timer = setTimeout(() => setDotsActive(true), 2000)
            if (data.error) {
                loadingState()
            } else {
                setCards(data)
                clearTimeout(timer)
                setDisabled(false)
                setFetching(false)
                setDotsActive(false)
            }
        }
    }, [data])

    const handlePagination = page => {
        const path = router.pathname
        const query = router.query
        query.page = page.selected + 1
        router.push({
            pathname: path,
            query: query,
        }, { shallow: true })
    }

    //формирование параметров запроса поиска
    const getFromApi = (e) => {
        e.preventDefault()
        const path = router.pathname
        const temp = {}
        if (query !== "") temp.search = query
        if (filterArea !== "") temp.opf = filterArea
        if (filterStatus !== "") temp.living_status = filterStatus
        router.push({
            pathname: path,
            query: temp,
        }, { shallow: true })
        selectedPage.current = 0
        setCards(temp)
    }


    function Paginate() {
        return (
            <div className={"pg"}>
                <ReactPaginate
                    previousLabel={"previous"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={1}
                    initialPage={data.selectedPage - 1}
                    pageCount={data.page}
                    onPageChange={handlePagination}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />
            </div>
        )
    }


    const animatedComponents = makeAnimated();
    const who = [
        { value: 'all', label: 'Все' },
        { value: 'person', label: 'Команды' },
        { value: 'teams', label: 'Участники' }
    ]

    const interests = [
        { value: '1', label: 'React.js' },
        { value: '2', label: 'Python' },
        { value: '3', label: 'Android' },
        { value: '4', label: 'React.js' },
        { value: '5', label: 'Python' },
        { value: '6', label: 'Android' }
    ]

    return (
        <div className={"teams-page"}>
            <FadeInSection key={1}>

                <form onSubmit={getFromApi} method="get">
                    <input name="s" placeholder="Поиск" type="search" onChange={e => setQuery(e.target.value)} />
                    <button type="submit"><FontAwesomeIcon icon={faSearch} /></button>
                </form>

                <div className="select">
                    <select>
                        <option>Участники</option>
                        <option>Команды</option>
                        <option>Все</option>
                    </select>
                    <div className="select__arrow" />
                </div>


                <div className={"title blue"}>Команды и участники</div>

                <div className={"blocks"}>
                    {data.results.map((data) => {
                        return (
                            <div className={"block-row"}>
                                <div className={"img-place"}>
                                    <div className={"type"}>
                                        <div className={"type-text"}>{data.theme}</div>
                                    </div>
                                    <img src={"/images/person/4.svg"} alt={""} />
                                </div>


                                <div className={"text-place"}>
                                    <div className={"text"}>{data.authors}</div>
                                    <div className={"text-subtitle"}>
                                        <span className={"blue"}>Последняя статья: </span> <b>{data.topic}</b>
                                    </div>

                                    <div className={"blocks"}>
                                        <div className={"block-work"}>
                                            <div className={"number"}>12</div>
                                            <div className={"text-t"}>Участий<br /> в конкурсах</div>
                                        </div>
                                        <div className={"block-work"}>
                                            <div className={"number"}>3</div>
                                            <div className={"text-t"}>Завершенных<br /> статей</div>
                                        </div>
                                        <div className={"block-work"}>
                                            <div className={"number"}>5</div>
                                            <div className={"text-t"}>Выигранных<br /> грантов</div>
                                        </div>
                                    </div>
                                </div>
                            </div>)
                    })}


                    <div className={"pagination"}>
                        <div className={"arrow"}>
                            <FontAwesomeIcon icon={faChevronLeft} />
                        </div>
                        <div className={"link active"}>
                            1
                        </div>
                        <div className={"link"}>
                            2
                        </div>
                        <div className={"link"}>
                            3
                        </div>
                        <div className={"link"}>
                            ...
                        </div>
                        <div className={"link"}>
                            50
                        </div>
                        <div className={"arrow active"}>
                            <FontAwesomeIcon icon={faChevronRight} />
                        </div>
                    </div>
                </div>

            </FadeInSection>
        </div>
    )
}

// People.getInitialProps = async ({ req }) => {
//     if (!req) {
//         return { data: null }
//     }
//     const response = await fetch('api/v1/rsue/')
//     const data = await response.json()

//     return {
//         data
//     }
// }