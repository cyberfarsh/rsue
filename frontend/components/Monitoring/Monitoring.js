import React from "react";
import FadeInSection from "../FadeIn/FadeIn";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function Monitoring() {
    return (
        <FadeInSection key={1}>
            <div className={"monitoring-page"}>
                <div className={"event-page"}>
                    <FadeInSection key={2}>
                        <div className={"title blue"}>Мониторинг</div>

                        <div className={"blocks"}>

                            <div className={"block-row"}>
                                <div className={"img-place"}>
                                    <img src={"/images/relaxation/1.svg"} alt={""}/>
                                </div>
                                <div className={"text-place"}>
                                    <div className={"text-t blue"}><a href="/team">CyberFarsh</a></div>
                                    <div className={"text-prize-t"}>
                                        Участвует в: <span className={"blue"}>Цифровой прорыв 2020  </span>
                                    </div>
                                    <div className={"about-t"}>
                                        Размер гранта: <span className="blue"> 750 000 Р</span>
                                    </div>
                                </div>

                                <div className={"date-a"}>
                                    <img src={"/images/icon/calendar.svg"} width={"20px"}
                                         height={"17px"}/> 20.11.2020 - 29.11.2020
                                </div>

                            </div>

                            <div className={"block-row"}>
                                <div className={"img-place"}>
                                    <img src={"/images/relaxation/2.svg"} alt={""}/>
                                </div>
                                <div className={"text-place"}>
                                    <div className={"text-t blue"}>SocialMayhem</div>
                                    <div className={"text-prize-t"}>
                                        Участвует в: <span className={"blue"}>Цифровой прорыв 2020  </span>
                                    </div>
                                    <div className={"about-t"}>
                                        Размер гранта: <span className="blue"> 1 000 000 - 1 500 000 Р</span>
                                    </div>
                                </div>

                                <div className={"date-a"}>
                                    <img src={"/images/icon/calendar.svg"} width={"20px"}
                                         height={"17px"}/> 20.11.2020 - 28.11.2020
                                </div>

                            </div>

                            <div className={"block-row"}>
                                <div className={"img-place"}>
                                    <img src={"/images/relaxation/3.svg"} alt={""}/>
                                </div>
                                <div className={"text-place"}>
                                    <div className={"text-t blue"}>PrettyGirls</div>
                                    <div className={"text-prize-t"}>
                                        Участвует в: <span className={"blue"}>Цифровой прорыв 2020  </span>
                                    </div>
                                    <div className={"about-t"}>
                                        Размер гранта: <span className="blue"> 1 000 000 - 1 500 000 Р</span>
                                    </div>
                                </div>

                                <div className={"date-a"}>
                                    <img src={"/images/icon/calendar.svg"} width={"20px"}
                                         height={"17px"}/> 20.11.2020 - 28.11.2020
                                </div>

                            </div>

                            <div className={"block-row"}>
                                <div className={"img-place"}>
                                    <img src={"/images/relaxation/1.svg"} alt={""}/>
                                </div>
                                <div className={"text-place"}>
                                    <div className={"text-t blue"}>ReactPower</div>
                                    <div className={"text-prize-t"}>
                                        Участвует в: <span className={"blue"}>Цифровой прорыв 2020  </span>
                                    </div>
                                    <div className={"about-t"}>
                                        Размер гранта: <span className="blue"> 1 000 000 - 1 500 000 Р</span>
                                    </div>
                                </div>

                                <div className={"date-a"}>
                                    <img src={"/images/icon/calendar.svg"} width={"20px"}
                                         height={"17px"}/> 20.11.2020 - 28.11.2020
                                </div>

                            </div>
                        </div>
                    </FadeInSection>
                </div>

            </div>
        </FadeInSection>
    )
}
