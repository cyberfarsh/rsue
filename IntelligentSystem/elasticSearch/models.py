from django.db import models


class dataModel(models.Model):
    UrlFile = models.TextField(blank=True, null=True)
    topic = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    annotation = models.TextField(blank=True, null=True)
    fulltext = models.TextField(blank=True, null=True)
    authors = models.TextField(blank=True, null=True)
    theme = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.topic
