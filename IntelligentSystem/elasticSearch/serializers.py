from rest_framework import serializers
from .documents import RsueDocument
from django_elasticsearch_dsl_drf.serializers import DocumentSerializer


class RsueSerializer(DocumentSerializer):
    class Meta(object):
        document = RsueDocument
        fields = (
            "UrlFile",
            "topic",
            "keywords",
            "annotation",
            "fulltext",
            "authors",
            "theme",
        )

