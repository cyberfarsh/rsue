from django.shortcuts import render
from rest_framework import generics, pagination
from .serializers import RsueSerializer
from .documents import RsueDocument
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.views.decorators.cache import never_cache
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    CompoundSearchFilterBackend,
)
from django_elasticsearch_dsl_drf.viewsets import DocumentViewSet
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from elasticsearch_dsl.query import Q
from elasticsearch_dsl import Index, A
import logging

logger = logging.getLogger(__name__)


class RsueList(DocumentViewSet):
    serializer_class = RsueSerializer
    document = RsueDocument
    filter_backends = [
        # FilteringFilterBackend,
        CompoundSearchFilterBackend,
    ]
    search_fields = (
        'fulltext',
    )

    def get_queryset(self):
        queryset = RsueDocument.search()
        queryset = queryset[:queryset.count()]
        return queryset

    @method_decorator(cache_page(60 * 60 * 2))
    def dispatch(self, *args, **kwargs):
        # if 'search' in self.request.GET:
        #     loginfo = self.request.GET['search']
        #     loginfo = loginfo.encode('utf-8')
        #     logger.info(loginfo)
        return super(RsueList, self).dispatch(*args, **kwargs)


@never_cache
@api_view(['POST', 'GET'])
def analytics(request):
    i = Index("article")
    s = i.search()
    sourceless_search = s.from_dict({
       "aggs": {
         "full_name": {
           "terms": {
             "field": "theme",
             "size": 10000
           }
         }
       },
       "size": 0
    })
    _query_response = sourceless_search.execute()
    result = _query_response.aggregations["full_name"]["buckets"]
    labels = [el["key"] for el in result]
    data = [el["doc_count"] for el in result]
    return Response({"labels": labels, "datas": data})



