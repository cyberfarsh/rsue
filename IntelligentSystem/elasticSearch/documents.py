from django_elasticsearch_dsl import Document, Index, fields
from .models import dataModel


# Вписать название индекса эластика
article = Index('article')

@article.doc_type
class RsueDocument(Document):
    UrlFile = fields.TextField(blank=True, null=True)
    topic = fields.TextField(blank=True, null=True)
    keywords = fields.TextField(blank=True, null=True)
    annotation = fields.TextField(blank=True, null=True)
    fulltext = fields.TextField(blank=True, null=True)
    authors = fields.TextField(blank=True, null=True)
    theme = fields.KeywordField(blank=True, null=True)
    class Django:
        model = dataModel
